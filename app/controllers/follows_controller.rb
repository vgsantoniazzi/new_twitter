﻿class FollowsController < ApplicationController
  before_filter :authenticate_user!

  def follow
    current_user.follow(user)
    redirect_to :back
  end

  def unfollow
    current_user.unfollow(user)
    redirect_to :back
  end

  private


  def user
    @user ||= User.find(params[:id])
  end
end
