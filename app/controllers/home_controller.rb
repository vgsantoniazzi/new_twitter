﻿class HomeController < ApplicationController
  def index
    @search = HomePresenter.new(params[:text])
  end
end
