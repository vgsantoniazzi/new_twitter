﻿class NotificationsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @notifications = current_user.notifications
  end

  def read_all
    current_user.notifications.read_all!
    head 200
  end
end
