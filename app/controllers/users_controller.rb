class UsersController < ApplicationController
  def index
    @users = User.all.map { |user| UserPresenter.new(user) }
  end

  def show
    @user = UserPresenter.new(user)
  end

  private

  def user
    User.friendly.find(params[:id])
  end
end
