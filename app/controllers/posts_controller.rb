class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  authorize_resource

  def index
    @posts = Post.all.collect { |post| PostPresenter.new(post) }
  end

  def show
    @post = PostPresenter.new(@post)
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = Post.new(post_params.merge(user: current_user))
    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      render :new
    end
  end

  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  private

  def set_post
    @post ||= Post.friendly.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :text)
  end
end
