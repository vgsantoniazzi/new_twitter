﻿class UserFollowNotifier
  attr_accessor :sender, :receiver

  def initialize(sender, receiver)
    @sender = sender
    @receiver = receiver
  end

  def save
    Notification.create(
      model: model,
      sender: sender,
      receiver: receiver,
      viewed: false
    )
  end

  private

  def model
    :follow
  end
end
