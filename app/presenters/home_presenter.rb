﻿class HomePresenter < SimpleDelegator
  attr_accessor :text

  def initialize(text)
    @text = text
    super(@text)
  end

  def posts
    PostSearch.new(text: text).search.collect do |post|
      PostPresenter.new(post)
    end
  end

  def users
    UserSearch.new(text: text).search.collect do |user|
      UserPresenter.new(user)
    end
  end
end
