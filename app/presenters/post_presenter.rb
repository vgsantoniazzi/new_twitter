﻿class PostPresenter < SimpleDelegator
  attr_accessor :post

  def initialize(post)
    @post = post
    super(@post)
  end

  def user_name
    user.name
  end

  def date
    post.created_at
  end

  def formatted_date
    date.to_s(:short)
  end

  def sanitized_text
    text.html_escape.html_safe
  end

  def omit_text_with
    I18n.t('read_more')
  end

  def user
    @user ||= post.user
  end
end
