﻿class UserPresenter < SimpleDelegator
  attr_accessor :user

  def initialize(user)
    @user = user
    super(@user)
  end

  def posts_count
    posts.count
  end

  def date_of_last_post
    PostPresenter.new(last_post).formatted_date
  end

  def last_post
    user.posts.order(:created_at).last
  end

  def follow_or_unfollow_link(current_user)
    helpers.link_to follow_or_unfollow_text(current_user),
                    follow_or_unfollow_path(current_user),
                    method: :post
  end

  def posts
    @posts ||= user.posts.collect { |post| PostPresenter.new(post) }
  end

  private

  def follow_or_unfollow_path(current_user)
    url.send(follow_or_unfollow(current_user) + '_users_path', user.id)
  end

  def follow_or_unfollow_text(current_user)
    t follow_or_unfollow(current_user)
  end

  def follow_or_unfollow(current_user)
    current_user.following.exists?(user.id) ? 'unfollow' : 'follow'
  end

  def t(key)
    I18n.t(key)
  end

  def url
    Rails.application.routes.url_helpers
  end

  def helpers
    ApplicationController.helpers
  end
end
