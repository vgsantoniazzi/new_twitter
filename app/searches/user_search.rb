﻿class UserSearch
  include ActiveModel::Model
  attr_accessor :text

  def search
    @search ||= find_users
  end

  private

  def find_users
    User.where(conditions).order("users.id")
  end

  def name_conditions
    ["unaccent(name) ILIKE ?", "%#{t text}%"] unless text.blank?
  end

  def nickname_conditions
    ["unaccent(nickname) ILIKE ?", "%#{t text}%"] unless text.blank?
  end

  def email_conditions
    ["unaccent(email) ILIKE ?", "%#{t text}%"] unless text.blank?
  end

  def conditions
    [conditions_clauses.join(' OR '), *conditions_options]
  end

  def conditions_clauses
    conditions_parts.map { |condition| condition.first }
  end

  def conditions_options
    conditions_parts.map { |condition| condition[1..-1] }.flatten
  end

  def conditions_parts
    private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
  end

  def t(string)
    I18n.transliterate(string)
  end
end
