﻿class PostSearch
  include ActiveModel::Model
  attr_accessor :text

  def search
    @search ||= find_posts
  end

  private

  def find_posts
    Post.where(conditions).order("posts.id")
  end

  def title_conditions
    ["unaccent(title) ILIKE ?", "%#{t text}%"] unless text.blank?
  end

  def text_conditions
    ["unaccent(text) ILIKE ?", "%#{t text}%"] unless text.blank?
  end

  def conditions
    [conditions_clauses.join(' OR '), *conditions_options]
  end

  def conditions_clauses
    conditions_parts.map { |condition| condition.first }
  end

  def conditions_options
    conditions_parts.map { |condition| condition[1..-1] }.flatten
  end

  def conditions_parts
    private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
  end

  def t(string)
    I18n.transliterate(string)
  end
end
