class Notification < ActiveRecord::Base
  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'

  def viewed?
    viewed
  end

  def self.read_all!
    update_all(viewed: true)
  end
end
