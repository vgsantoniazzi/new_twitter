class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Post
    can :manage, Post, user_id: user && user.id
  end
end
