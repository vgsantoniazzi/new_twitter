class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged]

  belongs_to :user

  validates :text, presence: true
  validates :title, presence: true
  validates :user, presence: true
end
