class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :nickname

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true
  validates :nickname, presence: true, uniqueness: true
  validates :email, presence: true

  has_many :follow_followers, class_name: 'Follow', foreign_key: :followed_id
  has_many :follow_followed, class_name: 'Follow', foreign_key: :follower_id

  has_many :notifications,
           class_name: 'Notification',
           foreign_key: :receiver_id,
           dependent: :destroy

  has_many :send_notifications,
           class_name: 'Notification',
           foreign_key: :sender_id,
           dependent: :destroy

  has_many :posts, dependent: :destroy

  has_many :followers, through: :follow_followers
  has_many :following, through: :follow_followed, source: :followed


  def unread_notifications?
    notifications.exists?(viewed: false)
  end

  def follow(user)
    following << user
    notify_follow(user)
  end

  def unfollow(user)
    following.delete(user)
  end

  private

  def notify_follow(receiver)
    UserFollowNotifier.new(self, receiver).save
  end
end
