﻿class String
  def html_escape
    self.gsub( %r{</?[^>]+?>}, '' )
  end
end
