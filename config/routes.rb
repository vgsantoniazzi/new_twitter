Rails.application.routes.draw do
  root to: 'home#index'
  devise_for :users, controllers: { registrations: 'registrations' }

  resources :users do
    collection do
      post '/follow/:id', action: :follow, controller: :follows, as: :follow
      post '/unfollow/:id', action: :unfollow, controller: :follows, as: :unfollow
    end
  end

  resources :posts

  resources :notifications, only: :index do
    put '/read_all', action: :read_all, on: :collection
  end

  resources :home, only: [:index]
end
