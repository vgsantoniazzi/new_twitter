class CreateFollows < ActiveRecord::Migration
  def change
    create_table :follows, id: false do |t|
      t.integer :follower_id
      t.integer :followed_id

      t.timestamps null: false
    end
    add_index(:follows, [:follower_id, :followed_id], unique: true)
    add_index(:follows, [:followed_id, :follower_id], unique: true)
  end
end
