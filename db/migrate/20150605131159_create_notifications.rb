class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :model
      t.integer :sender_id
      t.integer :receiver_id
      t.boolean :viewed

      t.timestamps null: false
    end
    add_index(:notifications, [:sender_id, :receiver_id])
    add_index(:notifications, [:receiver_id, :sender_id])
  end
end
