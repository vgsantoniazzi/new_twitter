﻿require 'rails_helper'

describe HomePresenter do
  let!(:user) { create(:user, name: 'Victor') }
  let!(:post) { create(:post, :with_user, title: 'victor') }

  let!(:presenter) { HomePresenter.new('victor') }

  context '#users' do
    it 'is a UserPresenter' do
      expect(presenter.users.first.is_a? UserPresenter).to eq(true)
    end

    it 'returns correct user object' do
      expect(presenter.users.first.user).to eq(user)
    end
  end

  context '#posts' do
    it 'is a PostPresenter' do
      expect(presenter.posts.first.is_a? PostPresenter).to eq(true)
    end

    it 'returns correct post object' do
      expect(presenter.posts.first.post).to eq(post)
    end
  end
end
