﻿require 'rails_helper'

describe UserPresenter do
  let!(:post) { create(:post, :with_user) }
  let!(:user) { post.user }

  let!(:presenter) { UserPresenter.new(user) }

  describe '#posts_count' do
    it { expect(presenter.posts_count).to eq(1) }
  end

  describe '#date_of_last_post' do
    it { expect(presenter.date_of_last_post).to eq(post.created_at.to_s(:short)) }
  end

  describe '#last_post' do
    it { expect(presenter.last_post).to eq(post) }
  end

  describe '#posts' do
    it { expect(presenter.posts.first.is_a? PostPresenter).to eq(true) }
  end

  describe '#follow_or_unfollow_button' do
    let!(:user_two) { create :user }

    context 'before follow' do
      it 'returns link to follow' do
        expect(presenter.follow_or_unfollow_link(user_two)).to eq(
          "<a rel=\"nofollow\" data-method=\"post\" href=\"/users/follow/#{user.id}\">Seguir</a>"
        )
      end
    end

    context 'after follow' do
      before do
        user_two.follow(user)
      end

      it 'returns link to unfollow' do
        expect(presenter.follow_or_unfollow_link(user_two)).to eq(
          "<a rel=\"nofollow\" data-method=\"post\" href=\"/users/unfollow/#{user.id}\">Deixar de seguir</a>"
        )
      end
    end
  end
end
