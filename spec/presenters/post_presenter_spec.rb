﻿require 'rails_helper'

describe PostPresenter do
  let!(:post) { create(:post, :with_user, text: "<p>lorem ipsun</p>") }
  let!(:user) { post.user }

  let!(:presenter) { PostPresenter.new(post) }

  describe '#user_name' do
    it { expect(presenter.user_name).to eq(user.name) }
  end

  describe '#date' do
    it { expect(presenter.date).to eq(post.created_at) }
  end
  describe '#formatted_date' do
    it { expect(presenter.formatted_date).to eq(post.created_at.to_s(:short)) }
  end

  describe '#sanitized_text' do
    it { expect(presenter.sanitized_text).to eq("lorem ipsun") }
  end

  describe '#omit_text_with' do
    it { expect(presenter.omit_text_with).to eq("[leia mais]") }
  end

  describe '#user' do
    it { expect(presenter.user).to eq(user) }
  end
end
