﻿describe Follow do
  context 'Associations' do
    it { should belong_to(:follower) }
    it { should belong_to(:followed) }
    it { should validate_uniqueness_of(:followed_id).scoped_to(:follower_id) }
  end
end
