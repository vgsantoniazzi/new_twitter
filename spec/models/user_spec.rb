describe User do
  context 'Associations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:nickname) }
    it { should validate_presence_of(:email) }

    it { should validate_uniqueness_of(:email) }
    it { should have_many :posts }

    describe '#unread_notifications?' do
      let!(:user_one) { create :user }
      let!(:user_two) { create :user }

      before do
        expect(user_two.unread_notifications?).to eq(false)
        user_one.follow(user_two)
      end

      it 'has one notification after follow' do
        expect(user_two.unread_notifications?).to eq(true)
      end

    end

    describe 'dependent: :destroy' do
      let!(:user_one) { create :user }
      let!(:user_two) { create :user }

      let!(:post) { create(:post, user: user_one) }

      before do
        user_one.follow(user_two)
      end

      context 'before destroy' do
        it 'user_one following one' do
          expect(user_one.following).to eq([user_two])
        end

        it 'user_two has one follower' do
          expect(user_two.followers).to eq([user_one])
        end

        it 'user two has one notification' do
          expect(user_two.notifications.count).to eq(1)
        end

        it 'user one has one post' do
          expect(user_one.posts).to eq([post])
        end
      end

      context 'after destroy' do
        before do
          user_one.destroy
          user_two.reload
        end

        it 'user_one raise error' do
          expect{ user_one.reload }.to raise_error
        end

        it 'user_two has one follower' do
          expect(user_two.followers).to eq([])
        end

        it 'user two has one notification' do
          expect(user_two.notifications.count).to eq(0)
        end

        it 'user one has one post' do
          expect(Post.count).to eq(0)
        end

      end
    end

    describe 'follows' do
      let!(:user_one) { create(:user) }
      let!(:user_two) { create(:user) }

      before do
        user_one.follow(user_two)
      end

      it 'user_one follow user_two' do
        expect(user_one.following).to eq([user_two])
      end

      it 'user_two was followed by user one' do
        expect(user_two.followers).to eq([user_one])
      end

      it 'user two not following user_one' do
        expect(user_two.following).to eq([])
      end

      it 'user one hasnt user two as follower' do
        expect(user_one.followers).to eq([])
      end

      it 'unfollow' do
        expect(user_one.unfollow(user_two)).to eq([user_two])
        expect(user_one.following).to eq([])
      end

      it 'unfollow unfollowed' do
        expect(user_one.unfollow(user_two)).to eq([user_two])
        expect(user_one.unfollow(user_two)).to eq([user_two])
        expect(user_one.following).to eq([])
      end

      describe 'notification' do
        let!(:notification) { user_two.notifications.first }

        it 'creates notification for user_two and not for user one' do
          expect(user_one.notifications.size).to eq(0)
          expect(user_two.notifications.size).to eq(1)
        end

        it 'creates an notification of follow model' do
          expect(notification.model).to eq("follow")
        end

        it 'creates an notification from user_one ' do
          expect(notification.sender).to eq(user_one)
        end

        it 'creates an notification to user_two' do
          expect(notification.receiver).to eq(user_two)
        end

        it 'creates an not viewed notificatioin' do
          expect(notification.viewed?).to eq(false)
        end
      end
    end
  end
end
