require 'rails_helper'

RSpec.describe Notification, type: :model do
  context 'Associations' do
    it { should belong_to :sender }
    it { should belong_to :receiver }
  end

  describe '#viewed?' do
    let!(:not_viewed) { create(:notification, viewed: false) }
    let!(:viewed) { create(:notification, viewed: true) }

    it 'respond correctly' do
      expect(not_viewed.viewed?).to eq(false)
      expect(viewed.viewed?).to eq(true)
    end
  end

  describe '#read_all' do
    let!(:user) { create(:user) }
    let!(:notification_one) { create(:notification, viewed: false, receiver: user) }
    let!(:notification_two) { create(:notification, viewed: false)}

    context 'by user' do
      before do
        expect(user.notifications.first.viewed?).to eq(false)
        user.notifications.read_all!
      end

      it 'read all notifications' do
        expect(user.notifications.first.viewed?).to eq(true)
      end

      it 'do not read other user notification' do
        expect(notification_two.viewed?).to eq(false)
      end
    end
  end
end
