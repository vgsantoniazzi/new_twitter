FactoryGirl.define do
  factory :notification do
    model "follow"
    viewed false

    trait :full do
      association :sender, factory: :user
      association :receiver, factory: :user
    end
  end
end
