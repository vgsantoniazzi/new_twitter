﻿require 'spec_helper'

describe UserSearch do
  describe '#name_conditions' do
    let!(:victor) { create(:user, name: 'Víctor antoniazzi') }
    let!(:john) { create(:user, name: 'John') }

    context 'unaccent and do not validate camel case' do
      before do
        @users = UserSearch.new(text: 'victor').search
      end

      it { expect(@users).to eq([victor]) }
    end
  end

  describe '#nickname_conditions' do
    let!(:victor) { create(:user, nickname: 'vgsantoniazzi') }
    let!(:antoniazzi) { create(:user, nickname: 'antoniazzi') }

    context 'unaccent and do not validate camel case' do
      before do
        @users = UserSearch.new(text: 'antoniazzi').search
      end

      it { expect(@users).to eq([victor, antoniazzi]) }
    end
  end

  describe '#email_conditions' do
    let!(:victor) { create(:user, email: 'vgsantoniazzi@gmail.com') }
    let!(:antoniazzi) { create(:user, email: 'antoniazzi@gmail.com') }

    context 'unaccent and do not validate camel case' do
      before do
        @users = UserSearch.new(text: 'santoniazzi').search
      end

      it { expect(@users).to eq([victor]) }
    end
  end

  context 'found in two fields' do
    let!(:victor) { create(:user, nickname: 'antoniazzi', email: 'vgsantoniazzi@gmail.com') }
    let!(:antoniazzi) { create(:user, nickname: 'noob', email: 'antoniazzi@gmail.com') }

    context 'unaccent and do not validate camel case' do
      before do
        @users = UserSearch.new(text: 'antoniazzi').search
      end

      it { expect(@users).to eq([victor, antoniazzi]) }
    end
  end
end
