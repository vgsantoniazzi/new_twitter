﻿require 'spec_helper'

describe PostSearch do
  describe '#text_conditions' do
    let!(:lorem) { create(:post, :with_user, text: 'Lorem Ipsun Dólor') }
    let!(:john) { create(:post, :with_user, text: 'John Dóe is developer') }

    context 'unaccent and do not validate camel case' do
      before do
        @posts = PostSearch.new(text: 'doe').search
      end

      it { expect(@posts).to eq([john]) }
    end
  end

  describe '#title_conditions' do
    let!(:doe) { create(:post, :with_user, title: 'John doe writer') }
    let!(:victor) { create(:post, :with_user, title: 'Víctor developer') }

    context 'unaccent and do not validate camel case' do
      before do
        @posts = PostSearch.new(text: 'victor').search
      end

      it { expect(@posts).to eq([victor]) }
    end
  end

  context 'found in two fields' do
    let!(:my_title) { create(:post, :with_user, title: 'My title', text: 'My text') }
    let!(:my_new_title) { create(:post, :with_user, title: 'My New Title', text: 'Awesome text') }

    context 'unaccent and do not validate camel case' do
      before do
        @posts = PostSearch.new(text: 'my').search
      end

      it { expect(@posts).to eq([my_title, my_new_title]) }
    end
  end
end
