module Features
  module SessionHelpers
    def sign_up_with(email, name, nickname, password, confirmation)
      visit new_user_registration_path
      fill_in 'Email', with: email
      fill_in 'Nome', with: name
      fill_in 'Apelido', with: nickname
      fill_in 'Senha', with: password
      fill_in 'Confirme sua senha', :with => confirmation
      click_button 'Registrar'
    end

    def signin(email, password)
      visit new_user_session_path
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      click_button 'Logar'
    end
  end
end
