﻿require 'rails_helper'

describe UserFollowNotifier do
  describe "#save!" do
    let!(:user_one) { create(:user) }
    let!(:user_two) { create(:user) }

    before do
      UserFollowNotifier.new(user_one, user_two).save
    end

    context 'validate notification data' do
      let!(:notification) { user_two.notifications.first! }

      it 'creates an notification for user_two' do
        expect(user_two.notifications.size).to eq(1)
        expect(user_one.notifications.size).to eq(0)
      end

      it 'creates and unviewed notification' do
        expect(notification.viewed?).to eq(false)
      end

      it 'returns correct model' do
        expect(notification.model).to eq('follow')
      end
    end
  end
end
