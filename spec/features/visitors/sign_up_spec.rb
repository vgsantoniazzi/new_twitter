feature 'Sign Up', :devise do
  scenario 'visitor can sign up with valid email address and password' do
    sign_up_with('test@example.com', 'Victor', 'vgsantoniazzi', 'please123', 'please123')
    txts = [I18n.t( 'devise.registrations.signed_up'), I18n.t( 'devise.registrations.signed_up_but_unconfirmed')]
    expect(page).to have_content(/.*#{txts[0]}.*|.*#{txts[1]}.*/)
  end

  scenario 'visitor cannot sign up with invalid email address' do
    sign_up_with('bogus', 'Bogus', 'bogus','please123', 'please123')
    expect(page).to have_content 'não é válido'
  end

  scenario 'visitor cannot sign up without password' do
    sign_up_with('test@example.com', 'Bogus', 'bogus', '', '')
    expect(page).to have_content "não pode ficar em branco"
  end

  scenario 'visitor cannot sign up with a short password' do
    sign_up_with('test@example.com', 'Bogus', 'bogus','please', 'please')
    expect(page).to have_content "é muito curto"
  end

  scenario 'visitor cannot sign up without password confirmation' do
    sign_up_with('test@example.com', 'Bogus', 'bogus', 'please123', '')
    expect(page).to have_content "não é igual a Password"
  end

  scenario 'visitor cannot sign up with mismatched password and confirmation' do
    sign_up_with('test@example.com', 'Bogus', 'bogus', 'please123', 'mismatch')
    expect(page).to have_content "não é igual a Password"
  end
end
