﻿include Warden::Test::Helpers
Warden.test_mode!

feature 'Search' do
  after do
    Warden.test_reset!
  end

  scenario 'Search show users and posts' do
    user = FactoryGirl.create(:user, name: 'John doe')
    post = FactoryGirl.create(:post, :with_user, title: 'John doe is the best')
    login_as(user, :scope => :user)

    visit user_path(user)
    fill_in 'text', with: 'john'
    click_button 'Procurar'
    expect(page).to have_content("Usuários John doe")
    expect(page).to have_content("Postagens John doe is the best")
  end
end
