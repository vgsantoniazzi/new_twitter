﻿include Warden::Test::Helpers
Warden.test_mode!

feature 'Follows' do
  after do
    Warden.test_reset!
  end

  scenario 'User dont has following and followers' do
    user = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
    visit user_path(user)
    expect(page).to have_content('Seguindo Ninguém Seguidores Ninguém')
  end

  scenario 'user follow another and creates an notification' do
    user = FactoryGirl.create(:user)
    user_two = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
    visit user_path(user_two)
    expect(page).to have_content('Seguindo Ninguém Seguidores Ninguém')

    expect(page).to have_no_content('Deixar de seguir')
    click_link 'Seguir'
    expect(page).to have_content('Deixar de seguir')

    click_link 'Notificações'
    expect(page).to have_content('Você não tem notificações')

    login_as(user_two, :scope => :user)
    visit notifications_path
    expect(page).to have_content("O usuário #{user.name} está seguindo você")
  end

  scenario 'user follow and unfollow' do
    user = FactoryGirl.create(:user)
    user_two = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
    visit user_path(user_two)
    expect(page).to have_content('Seguindo Ninguém Seguidores Ninguém')

    expect(page).to have_no_content('Deixar de seguir')
    click_link 'Seguir'
    expect(page).to have_content('Deixar de seguir')
    click_link 'Deixar de seguir'
    expect(page).to have_content 'Seguir'

    click_link 'Notificações'
    expect(page).to have_content('Você não tem notificações')

    login_as(user_two, :scope => :user)
    visit notifications_path
    expect(page).to have_content("O usuário #{user.name} está seguindo você")

  end
end
