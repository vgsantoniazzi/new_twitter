﻿include Warden::Test::Helpers
Warden.test_mode!

feature 'Posts' do
  after do
    Warden.test_reset!
  end

  scenario 'User login and create post' do
    user = FactoryGirl.create(:user)
    login_as(user, scope: :user)
    visit user_path(user)
    click_link 'Postagens'

    click_button 'Nova'

    fill_in 'Título', with: ''
    fill_in 'Texto', with: 'Lorem ipsun'
    click_button 'Salvar'
    expect(page).to have_content('não pode ficar em branco')

    fill_in 'Título', with: 'Nova postagem'
    click_button 'Salvar'
    expect(page).to have_content("Postagens")
    expect(page).to have_content("Nova postagem")

    visit user_path(user)
    expect(page).to have_content("Postagens: 1")
  end

  scenario 'Edit post' do
    user = FactoryGirl.create(:user)
    post = FactoryGirl.create(:post, user: user, title: 'firstpost')
    login_as(user, scope: :user)

    visit user_path(user)
    click_link('Postagens')
    click_link('firstpost')
    click_button('Editar')

    fill_in 'Título', with: ''
    click_button 'Salvar'
    expect(page).to have_content('não pode ficar em branco')

    fill_in 'Título', with: 'changefirst'
    click_button 'Salvar'
    expect(page).to have_content("Postagens")
    expect(page).to have_content("changefirst")
  end


  scenario 'Remove post' do
    user = FactoryGirl.create(:user)
    post = FactoryGirl.create(:post, user: user, title: 'firstpost')

    login_as(user, scope: :user)

    visit user_path(user)
    click_link('Postagens')
    click_link('firstpost')
    click_button('Deletar')

    visit user_path(user)
    expect(page).to have_content('Postagens: 0')
  end
end
