New twitter, or medium or not
================

Application example to apply a job on HE:Labs.

Microblog like medium. With posts, follow and notifications.

How to run
-----------

Create database, execute migrations and start the server

``` ruby
bundle install

rake db:create

rake db:migrate

rails s
```

Ruby on Rails
-------------

This application requires:

- Ruby 2.2.0
- Rails 4.2.0
- Postgresql 9.3+ with unaccent


Credits
-------
Victor Antoniazzi <vgsantoniazzi@gmail.com>

